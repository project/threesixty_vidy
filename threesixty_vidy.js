(function ($) {

// Valiant360 behavior code
  Drupal.behaviors.Threesixty_vidy = {
    attach: function() {
      $('.threesixty_vidy_container').Valiant360(
        {
          clickAndDrag: Drupal.settings.Threesixty_vidy.clickAndDrag,
          hideControls: Drupal.settings.Threesixty_vidy.hideControls,
          muted: Drupal.settings.Threesixty_vidy.muted,
          autoplay: Drupal.settings.Threesixty_vidy.autoplay
        }
      );
    }
  }
}(jQuery));
